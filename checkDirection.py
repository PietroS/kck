def checkDirection(toDo, lastDirection):
    if(toDo[0] != 'null'):
        if (toDo[2] == 'null' and lastDirection == 'null'):
            toDo[2] = 'UP'
            lastDirection = toDo[2]
        elif (toDo[2] == 'null' and lastDirection != 'null'):
            toDo[2] = lastDirection
        elif (toDo[2] != 'null' and lastDirection == 'null'):
            lastDirection = toDo[2]
        else: #(toDo[2] != 'null' and lastDirection != 'null'):
            lastDirection = toDo[2]
            toDo.append(lastDirection)
    else:
        toDo.append(lastDirection)
    return toDo