from Mina import Mina
import time
from random import randrange

class Plansza:
    tab = [[0 for col in range(10)] for row in range(10)]
    tab_start = [[0 for col in range(10)] for row in range(10)]
    #RX/Y wsp�rz�dne robocika
    RX, RY = 9,9;
    #    0  1  2  3  4
    # 0 [ ][ ][ ][ ][ ]
    # 1 [ ][ ][ ][ ][ ]
    # 2 [ ][ ][ ][ ][ ]
    # 3 [ ][ ][ ][ ][ ]
    # 4 [ ][ ][ ][ ][ ]

    def wypelnij_minami(self):
        print("[---------[DEBUG #START]----------]")
        ilosc_min = 25
        licznik = 1
        for x in range(0, 10):
            for y in range(0, 10):
                if ((randrange(100))%2 == 0) & (ilosc_min != 0):
                    self.tab[x][y] = "Mina"
                    ilosc_min = ilosc_min - 1
                    print("[", self.tab[x][y], "]", end="")
                else:
                    self.tab[x][y] = "Safe"
                    print("[", self.tab[x][y], "]", end="")
            print(" ")
        print("[----------[DEBUG #STOP]----------]")
        self.wypelnij_puste()
        self.spawn_robot()
        self.display()

    def wypelnij_puste(self):
        for x in range(0, 10):
            for y in range(0, 10):
                    self.tab_start[x][y] = " "
        print(" ")

    def spawn_robot(self):
        self.tab_start[self.RX][self.RY] = "R"

    def update_robot_pos(self):
        self.tab_start[self.RX][self.RY] = "R"

    def display(self):
        print()
        for x in range(0, 10):
            for y in range(0, 10):
                    print("[", self.tab_start[x][y], "]", end="")
            print()
        print()

#TODO: Sprawdzanie czy nie wychodzi za granice + Kierunek patrzenia(to przy rozbrajaniu)

    def update_ruch(self):
        self.check_4_bomb()
        self.update_robot_pos()
        #self.display(self)

    def move_up(self):
        self.tab_start[self.RX][self.RY] = "x"
        self.RX = self.RX - 1
        self.RY = self.RY
        self.update_ruch()

    def move_down(self):
        self.tab_start[self.RX][self.RY] = "x"
        self.RX = self.RX + 1
        self.RY = self.RY
        self.update_ruch()

    def move_left(self):
        self.tab_start[self.RX][self.RY] = "x"
        self.RY = self.RY - 1
        self.update_ruch()

    def move_right(self):
        self.tab_start[self.RX][self.RY] = "x"
        self.RX = self.RX
        self.RY = self.RY + 1
        self.update_ruch()

    def check_4_bomb(self):
        if (self.tab[self.RX][self.RY] == "Mina"):
            self.tab_start[self.RX][self.RY] = "!"
            self.display()
            print('Chyba w cos wdepnalem')
            time.sleep(5)
            print('KBOOOOOOM!')
            exit(0)