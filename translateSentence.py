def replacePolishCharsWithNormalChars(s):
    polishChars = ['ą','ż','ź','ś','ć','ę','ń','ó','ł']
    normalChars = ['a','z','z','s','c','e','n','o','l']
    for i in range(len(s)):
        for j in range(len(polishChars)):
            if(s[i] == polishChars[j]):
                s = s.replace(polishChars[j],normalChars[j])
                break
    return s

def translateSentence(sentence, dictionary):

    s = replacePolishCharsWithNormalChars(sentence)
    s = s.lower().replace(',', ' ').split(' ')

    toExecute = []

    for i in range(len(s)):
        word = s[i]
        meanings = dictionary.searchTypeAndMeaningForWord(word)
        toExecute.append(meanings)

    return toExecute