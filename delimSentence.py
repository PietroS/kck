def copyTab(origin,fromPos,toPos):
    tab = []
    for i in range(fromPos,toPos):
        tab.append(origin[i])
    return tab

def delimSentence(toExecute):
    delimSentences = []

    # Pamietanie pozycji spojnikow
    pozSpoj = []
    lastPos = 0
    for i in range (len(toExecute)):
        if (toExecute[i][0][1] == "LINK"):
            if(len(pozSpoj) == 0):
                delimSentences.append(copyTab(toExecute,0,i))
            else:
                if((i - pozSpoj[len(pozSpoj)-1]) > 1):
                    delimSentences.append(copyTab(toExecute,pozSpoj[len(pozSpoj) - 1] + 1,i))
            pozSpoj.append(i)
        lastPos = i
    if(len(pozSpoj) != 0 and lastPos - pozSpoj[len(pozSpoj) - 1] > 1):
        delimSentences.append(copyTab(toExecute,pozSpoj[len(pozSpoj) - 1] + 1,len(toExecute)))
    if(len(delimSentences) == 0):
        delimSentences.append(toExecute)
    #print(delimSentences)
    return(delimSentences)