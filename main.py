from Logs import Logs
from Responds import Responds
from Dictionary import Dictionary
from translateSentence import translateSentence
from interpretSentence import interpretSentence
from Saper import Saper
from checkDirection import checkDirection
from Plansza import Plansza
from executeOrder import executeOrder
from delimSentence import delimSentence

def printWhatInterpret(toDo):
    print("Otrzymano rozkaz typu: " + toDo[0])
    print("Z argumentami: " + str(toDo[1]))
    print("Kierunek: " + toDo[2])

dictionary = Dictionary()
dictionary.__init__()
plansza = Plansza()
plansza.wypelnij_minami()
responds = Responds()
responds.__init__()
logs = Logs()
logs.__init__()

while True:
    Saper.leadDialog()
    respond = input(">>")
    toExecute = translateSentence(respond,dictionary)
    toExecute = delimSentence(toExecute)
    saperRespond = []
    for i in toExecute:
        toDo = interpretSentence(i)
        toDo = checkDirection(toDo,logs.searchLastDirection())
        executeOrder(toDo,plansza,logs)
        logs.append(toDo)
        if (toDo[0] != 'null'):
            plansza.display()
        saperRespond.append(Saper.respondDialog(toDo,responds))
    for i in saperRespond:
        print(i)