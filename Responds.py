class Responds:
    respondsCommandsTab = []
    respondsTab = []

    def __init__(self):
        all = []
        try:
            respondsFile = open('responds.tsv', 'r', encoding='utf-8')
            all = respondsFile.readlines()
            respondsFile.close()
        except:
            print("Brak pliku z odpowiedziami w folderze programu!")
            exit(1)
        for i in range(len(all)):
            tab = []
            all[i] = all[i].replace('\n', '').split('\t')
            for j in range(len(all[i])-1):
                 tab.append(all[i][j])
            Responds.respondsCommandsTab.append(tab)
            Responds.respondsTab.append(all[i][len(all[i])-1])

    def searchResponse(self,todo):
        for i in range(len(Responds.respondsCommandsTab)):
            count = 0
            for j in range(len(todo)):
                if(todo[j] in Responds.respondsCommandsTab[i]):
                    count += 1
                if(count == len(Responds.respondsCommandsTab[i])):
                    return Responds.respondsTab[i]
        return False

