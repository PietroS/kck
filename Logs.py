class Logs:
    undoneLogs = []
    logsTab = []
    movementLogs = []

    def __init__(self):
        open('logs.txt', 'w').close()

    def append(self, toDo):
        logs = open('logs.txt', 'a')
        logs.write("{0} {1} {2}\n".format(str(toDo[0]), str(toDo[1][0]), str(toDo[2])))
        logs.close()
        if(toDo[0] == "GO"):
            Logs.__keepRightOrder(self)

    def __keepRightOrder(self):
        for i in range(len(Logs.undoneLogs)):
            Logs.undoneLogs[i] = Logs.undoneLogs[i] + 1

    def __read(self):
        file = open('logs.txt','r')
        logs = file.readlines()
        logs.reverse()
        file.close()
        return logs

    def list(self):
        return Logs.__read(self)

    def listReversed(self):
        logs = Logs.__read(self).reverse(self)
        return logs

    def returnPosOfFristSpotted(self,order):
        logs = Logs.__read(self)
        for i in range(len(logs)):
            if(order in logs[i]):
                return i
        return 0

    def returnPosOfFristSpottedReversed(self,order):
        logs = Logs.listReversed()
        for i in range(len(logs)):
            if(order in logs[i]):
                return i + len(Logs.undoneLogs)
        return 0

    def __filterBy(self,order,logs):
        filteredLogs = []
        for i in range(len(logs)):
            if(logs[i][0] == order):
                filteredLogs.append(logs[i])
        return filteredLogs

    def debug(self):
        print("undoneLogs: ")
        print(Logs.undoneLogs)
        print("__read: ")
        print(Logs.__read(self))
        print("withoutUndone: ")
        print(Logs.searchWithoutUndone(self,'GO'))

    def search(self, order):
        tab = []
        logs = Logs.__read(self)
        for i in range(len(logs)):
            if(order in logs[i]):
                logs[i] = logs[i].replace('\n', '').split(' ')
                tab.append(logs[i])
        return tab

    def searchWithoutUndone(self,order):
        logs = Logs.search(Logs,order)
        logsWithoutUndone = []
        for i in range(len(logs)):
            if(i not in Logs.undoneLogs):
                logsWithoutUndone.append(logs[i])
        return logsWithoutUndone

    def searchLastDirection(self):
        tab = []
        tabDir = ['UP','DOWN','LEFT','RIGHT']
        logs = Logs.__read(Logs)
        for log in logs:
            for dir in tabDir:
                if(dir in log):
                    return dir
        return 'UP'
