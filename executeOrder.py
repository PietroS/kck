from Plansza import Plansza
from Logs import Logs

def reverseDir(dir):
    tab = {'UP':'DOWN','DOWN':'UP', 'LEFT':'RIGHT', 'RIGHT':'LEFT'}
    return tab[dir]

def doMovementOnBoard(direction, board):
    if (direction == 'UP'):
        board.move_up()
    if (direction == 'DOWN'):
        board.move_down()
    if (direction == 'LEFT'):
        board.move_left()
    if (direction == 'RIGHT'):
        board.move_right()

def executeOrder(toDo, plansza, logs):
    if(toDo[0] == 'GO'):
        for i in range(toDo[1][0]):
            doMovementOnBoard(toDo[2],plansza)
    if(toDo[0] == 'GOBACK'):
        reversedDir = reverseDir(toDo[2])
        for i in range(toDo[1][0]):
            doMovementOnBoard(reversedDir, plansza)
    if(toDo[0] == 'UNDO'):
        undoLogs = logs.searchWithoutUndone('GO')
        print(undoLogs)
        if (toDo[1][0] > len(undoLogs)):
            toDo[1][0] = len(undoLogs)
        count = 0 + logs.returnPosOfFristSpotted('GO')
        for i in range(int(toDo[1][0])):
            #log = log.replace('\n','').split(' ')
            logs.debug()
            reversedDir = reverseDir(undoLogs[i][2])
            for i in range(int(undoLogs[i][1])):
                if (reversedDir == 'UP'):
                    plansza.move_up()
                if (reversedDir == 'DOWN'):
                    plansza.move_down()
                if (reversedDir == 'LEFT'):
                    plansza.move_left()
                if (reversedDir == 'RIGHT'):
                    plansza.move_right()
            logs.undoneLogs.append(count)
            count = count + 1



