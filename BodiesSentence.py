class BodiesSentence:

    bodies = []

    def __init__():
        bodies = []
        try:
            bodiesFile = open('dictionary.tsv', 'r')
            bodies = bodiesFile.readlines()
            bodiesFile.close()
        except:
            print("Brak pliku z korpusami zdań w folderze programu!")
            exit(1)
        for i in range(len(bodies)):
            bodies[i] = bodies[i].replace('\n', '')
        BodiesSentence.bodies = bodies